<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses'=>'LotteryController@lottery','as'=>'lottery-results']);
Route::post('/search-lottery', ['uses'=>'LotteryController@searchLottery','as'=>'searchLottery']);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('login', ['uses' => 'Voyager\VoyagerBaseController@login', 'as' => 'voyager.login']);
    
    Route::get('working-schedules/create', ['uses' => 'Voyager\VoyagerBaseController@workingSchedule', 'as' => 'voyager.working-schedules.create']);

    Route::post('store-working-schedule', ['uses' => 'Voyager\VoyagerBaseController@addWorkingSchedule', 'as' => 'voyager.add-working-schedule.store']);
    
    Route::post('get-employees', ['uses' => 'Voyager\VoyagerBaseController@getEmployeesByEmpId', 'as' => 'voyager.getEmpByEmpId.view']);
	
	Route::post('charges', ['uses' => 'Voyager\VoyagerBaseController@addChargeTransaction', 'as' => 'voyager.charges.store']);
	
	Route::get('transactions/{id}', ['uses' => 'Voyager\VoyagerBaseController@viewTransactions', 'as' => 'voyager.add-transactions.view']);
	
	Route::post('transaction', ['uses' => 'Voyager\VoyagerBaseController@addTransaction', 'as' => 'voyager.charges-transaction.store']);
	
	Route::get('hardware-reslove/{id}', ['uses' => 'Voyager\VoyagerBaseController@hardwareResolve', 'as' => 'voyager.hardware-reslove.view']);
	
	Route::post('resolve-problem', ['uses' => 'Voyager\VoyagerBaseController@resolveHardwareProblem', 'as' => 'voyager.resolved-hardware-problem.store']);

	//Route::get('working-schedules', ['uses' => 'Voyager\VoyagerBaseController@workingScheduleList', 'as' => 'voyager.workingSchedule.view']);
});
