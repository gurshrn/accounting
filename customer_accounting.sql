-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 08, 2019 at 06:48 AM
-- Server version: 5.6.41
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer_accounting`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `charges`
--

CREATE TABLE `charges` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `needs_type` enum('Personal Needs','Office Needs') COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` float NOT NULL,
  `paid_amount` float NOT NULL,
  `date` date NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `charges`
--

INSERT INTO `charges` (`id`, `emp_id`, `location_id`, `needs_type`, `total_amount`, `paid_amount`, `date`, `comment`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Personal Needs', 23, 23, '2018-11-01', 'test', 2, NULL, '2018-11-16 00:39:46', '2018-11-16 00:39:46'),
(2, 1, 2, 'Personal Needs', 23, 23, '2018-01-01', 'test', 2, NULL, '2018-11-16 02:23:17', '2018-11-16 02:23:17'),
(3, 1, 2, 'Personal Needs', 23, 23, '2018-01-01', 'test', 2, NULL, '2018-11-16 02:24:33', '2018-11-16 02:24:33'),
(4, 1, 2, 'Personal Needs', 23, 23, '2018-01-01', 'test', 2, NULL, '2018-11-16 02:25:19', '2018-11-16 02:25:19'),
(5, 1, 2, 'Personal Needs', 23, 23, '2018-01-01', 'test', 2, NULL, '2018-11-16 02:26:23', '2018-11-16 02:26:23'),
(6, 1, 2, 'Personal Needs', 23, 23, '2018-01-01', 'test', 2, NULL, '2018-11-16 02:26:32', '2018-11-16 02:26:32'),
(7, 1, 2, 'Personal Needs', 34, 12, '2018-01-31', 'test', 2, NULL, '2018-11-19 05:01:00', '2018-11-19 05:01:00'),
(8, 1, 2, 'Personal Needs', 34, 12, '2017-10-30', 'test', 2, NULL, '2018-11-19 05:01:18', '2018-11-19 05:01:18'),
(9, 1, 2, 'Personal Needs', 34, 12, '2017-10-30', 'test', 2, NULL, '2018-11-19 05:01:18', '2018-11-19 05:01:18'),
(10, 1, 2, 'Personal Needs', 34, 12, '2017-10-30', 'test', 2, NULL, '2018-11-19 05:01:56', '2018-11-19 05:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `charge_transactions`
--

CREATE TABLE `charge_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `charges_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `paid_amount` float NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `charge_transactions`
--

INSERT INTO `charge_transactions` (`id`, `charges_id`, `date`, `paid_amount`, `comment`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 4, '0000-00-00', 23, 'test', NULL, '2018-11-16 02:25:19', '2018-11-16 02:25:19'),
(2, 6, '0000-00-00', 10, 'test', NULL, '2018-11-16 02:26:23', '2018-11-16 02:26:23'),
(4, 6, '2018-12-31', 10, 'test', NULL, '2018-11-19 02:10:27', '2018-11-19 02:10:27'),
(6, 6, '2018-11-19', 3, 'test', NULL, '2018-11-19 04:09:36', '2018-11-19 04:09:36'),
(7, 10, '2017-10-30', 12, 'test', NULL, '2018-11-19 05:01:56', '2018-11-19 05:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(49, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(52, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(57, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(58, 8, 'location_name', 'text', 'Location Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(59, 8, 'pos_id', 'text', 'Pos Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(60, 8, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 4),
(61, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 5),
(62, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(63, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(64, 10, 'emp_type', 'text', 'Emp Type', 1, 1, 1, 1, 1, 1, '{}', 2),
(65, 10, 'added_by', 'text', 'Added By', 1, 0, 0, 0, 0, 0, '{}', 3),
(66, 10, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 4),
(67, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 5),
(68, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(69, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(70, 11, 'emp_id', 'text', 'Employee Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(71, 11, 'emp_type_id', 'select_dropdown', 'Emp Type Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(72, 11, 'emp_name', 'text', 'Employee Name', 1, 1, 1, 1, 1, 1, '{}', 5),
(73, 11, 'gender', 'select_dropdown', 'Gender', 1, 1, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"_empty_\":\"-- None --\",\"Female\":\"Female\",\"Male\":\"Male\"}}', 6),
(74, 11, 'date_of_birth', 'date', 'Date Of Birth', 1, 1, 1, 1, 1, 1, '{}', 7),
(75, 11, 'place_of_birth', 'text', 'Place Of Birth', 1, 1, 1, 1, 1, 1, '{}', 8),
(76, 11, 'nationality', 'text', 'Nationality', 1, 1, 1, 1, 1, 1, '{}', 9),
(77, 11, 'address', 'text', 'Address', 1, 1, 1, 1, 1, 1, '{}', 10),
(78, 11, 'job_title', 'text', 'Job Title', 1, 1, 1, 1, 1, 1, '{}', 11),
(79, 11, 'wage', 'text', 'Wage', 1, 1, 1, 1, 1, 1, '{}', 12),
(81, 11, 'attached_doc', 'image', 'Attached Document', 1, 1, 1, 1, 1, 1, '{}', 14),
(82, 11, 'spouse_name', 'text', 'Spouse Name', 1, 1, 1, 1, 1, 1, '{}', 15),
(83, 11, 'spouse_date_of_birth', 'date', 'Spouse Date Of Birth', 1, 1, 1, 1, 1, 1, '{}', 16),
(84, 11, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 17),
(85, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 18),
(86, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 19),
(87, 11, 'employee_hasone_employee_type_relationship', 'relationship', 'Employee Type', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\EmployeeType\",\"table\":\"employee_types\",\"type\":\"belongsTo\",\"column\":\"emp_type_id\",\"key\":\"id\",\"label\":\"emp_type\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(88, 11, 'wage_type', 'radio_btn', 'Wage Type', 1, 1, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"Monthly\":\"Monthly\",\"By Monthly\":\"By Monthly\"}}', 11),
(89, 11, 'marital_status', 'select_dropdown', 'Marital Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"_empty_\":\"-- None --\",\"Married\":\"Married\",\"Single\":\"Single\",\"Divorced\":\"Divorced\",\"Widowed\":\"Widowed\"}}', 13),
(90, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(91, 13, 'emp_id', 'text', 'Emp Id', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 2),
(92, 13, 'location_id', 'text', 'Location Id', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 5),
(94, 13, 'needs', 'text', 'Needs', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 6),
(95, 13, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 7),
(96, 13, 'added_by', 'text', 'Added By', 1, 0, 0, 0, 0, 0, '{}', 9),
(97, 13, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 10),
(98, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 11),
(99, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(100, 13, 'need_belongsto_employee_relationship', 'relationship', 'Employee', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Employee\",\"table\":\"employees\",\"type\":\"belongsTo\",\"column\":\"emp_id\",\"key\":\"id\",\"label\":\"emp_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(101, 13, 'need_belongsto_location_relationship', 'relationship', 'Location', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"location_id\",\"key\":\"id\",\"label\":\"location_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(102, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(103, 14, 'emp_id', 'select_dropdown', 'Emp Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(104, 14, 'location_id', 'select_dropdown', 'Location Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(106, 14, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 7),
(107, 14, 'added_by', 'text_area', 'Added By', 1, 0, 0, 0, 0, 0, '{}', 9),
(108, 14, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 10),
(109, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 11),
(110, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(111, 14, 'sale_belongsto_employee_relationship', 'relationship', 'Employee', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Employee\",\"table\":\"employees\",\"type\":\"belongsTo\",\"column\":\"emp_id\",\"key\":\"id\",\"label\":\"emp_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(112, 14, 'sale_belongsto_location_relationship', 'relationship', 'Location', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"location_id\",\"key\":\"id\",\"label\":\"location_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(113, 13, 'comment', 'text_area', 'Comment', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"12\"}}', 8),
(114, 14, 'comment', 'text_area', 'Comment', 1, 1, 1, 1, 1, 1, '{}', 8),
(115, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(116, 15, 'hw_id', 'select_dropdown', 'Hardware Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(117, 15, 'location_id', 'select_dropdown', 'Location Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(118, 15, 'problem_description', 'text_area', 'Problem Description', 1, 1, 1, 1, 1, 1, '{}', 6),
(119, 15, 'date', 'date', 'Date', 1, 1, 1, 1, 1, 1, '{}', 7),
(120, 15, 'resolve', 'radio_btn', 'Resolve', 1, 0, 0, 0, 0, 0, '{\"default\":\"\",\"options\":{\"Yes\":\"Yes\",\"No\":\"No\"}}', 8),
(121, 15, 'resolve_date', 'date', 'Resolve Date', 1, 0, 0, 0, 0, 0, '{}', 9),
(122, 15, 'tech_info', 'text_area', 'Technical Information', 1, 0, 0, 0, 0, 0, '{}', 10),
(123, 15, 'resolved_by', 'text', 'Resolved By', 1, 0, 0, 0, 0, 0, '{}', 11),
(124, 15, 'added_by', 'text', 'Added By', 1, 0, 0, 0, 0, 0, '{}', 12),
(125, 15, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 13),
(126, 15, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 1, 0, 0, '{}', 14),
(127, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(128, 15, 'hardware_maintenance_belongsto_location_relationship', 'relationship', 'Location', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"location_id\",\"key\":\"id\",\"label\":\"location_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(129, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(130, 16, 'hw_id', 'text', 'Hardware Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(131, 16, 'device', 'text', 'Device', 1, 1, 1, 1, 1, 1, '{}', 3),
(132, 16, 'location_id', 'select_dropdown', 'Location Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(133, 16, 'description', 'text_area', 'Description', 1, 1, 1, 1, 1, 1, '{}', 8),
(134, 16, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 6),
(135, 16, 'date_of_purchase', 'date', 'Date Of Purchase', 1, 1, 1, 1, 1, 1, '{}', 7),
(136, 16, 'added_by', 'text', 'Added By', 1, 0, 0, 0, 0, 0, '{}', 9),
(137, 16, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 10),
(138, 16, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 11),
(139, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(140, 16, 'hardware_registration_belongsto_location_relationship', 'relationship', 'Location', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"location_id\",\"key\":\"id\",\"label\":\"location_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(141, 15, 'hardware_maintenance_belongsto_hardware_registration_relationship', 'relationship', 'Hardware Id', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\HardwareRegistration\",\"table\":\"hardware_registrations\",\"type\":\"belongsTo\",\"column\":\"hw_id\",\"key\":\"id\",\"label\":\"hw_id\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(142, 13, 'date', 'date', 'Date', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 6),
(143, 14, 'date', 'date', 'Date', 1, 1, 1, 1, 1, 1, '{}', 6),
(144, 13, 'status', 'text', 'Status', 1, 0, 0, 0, 0, 0, '{\"display\":{\"width\":\"4\"}}', 9),
(145, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(146, 17, 'emp_id', 'select_dropdown', 'Emp Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(147, 17, 'location_id', 'select_dropdown', 'Location Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(148, 17, 'needs_type', 'select_dropdown', 'Needs Type', 1, 1, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"_empty_\":\"-- None --\",\"Personal Needs\":\"Personal Needs\",\"Office Needs\":\"Office Needs\"}}', 6),
(149, 17, 'total_amount', 'text', 'Total Amount', 1, 1, 1, 1, 1, 1, '{}', 7),
(150, 17, 'paid_amount', 'text', 'Paid Amount', 1, 1, 1, 1, 1, 1, '{}', 8),
(151, 17, 'date', 'date', 'Date', 1, 1, 1, 1, 1, 1, '{}', 9),
(152, 17, 'comment', 'text_area', 'Comment', 1, 1, 1, 1, 1, 1, '{}', 10),
(153, 17, 'status', 'text', 'Status', 1, 0, 0, 0, 0, 0, '{}', 11),
(154, 17, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 12),
(155, 17, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 14),
(156, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(157, 17, 'added_by', 'text', 'Added By', 1, 0, 0, 0, 0, 0, '{}', 13),
(158, 17, 'charge_belongsto_employee_relationship', 'relationship', 'Employee', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Employee\",\"table\":\"employees\",\"type\":\"belongsTo\",\"column\":\"emp_id\",\"key\":\"id\",\"label\":\"emp_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(159, 17, 'charge_belongsto_location_relationship', 'relationship', 'Location', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"location_id\",\"key\":\"id\",\"label\":\"location_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(160, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(161, 18, 'location_id', 'select_dropdown', 'Location Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(162, 18, 'date', 'date', 'Date', 1, 1, 1, 1, 1, 1, '{}', 4),
(163, 18, 'lottery_number', 'text', 'Lottery Number', 1, 1, 1, 1, 1, 1, '{}', 5),
(164, 18, 'added_by', 'text', 'Added By', 1, 0, 0, 0, 0, 0, '{}', 6),
(165, 18, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 7),
(166, 18, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 8),
(167, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(168, 18, 'lottery_result_belongsto_location_relationship', 'relationship', 'locations', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"location_id\",\"key\":\"id\",\"label\":\"location_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-11-10 04:57:49', '2018-11-10 04:57:49'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-11-10 04:57:49', '2018-11-10 04:57:49'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-11-10 04:57:49', '2018-11-10 04:57:49'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-11-10 04:58:01', '2018-11-10 04:58:01'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2018-11-10 04:58:02', '2018-11-10 04:58:02'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2018-11-10 04:58:04', '2018-11-10 04:58:04'),
(8, 'locations', 'locations', 'Location', 'Locations', NULL, 'App\\Models\\Location', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-14 01:37:35', '2018-11-14 01:37:35'),
(10, 'employee_types', 'employee-types', 'Employee Type', 'Employee Types', NULL, 'App\\Models\\EmployeeType', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-14 05:09:21', '2018-11-14 05:09:21'),
(11, 'employees', 'employees', 'Employee', 'Employees', NULL, 'App\\Models\\Employee', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-14 23:08:45', '2018-11-15 00:47:36'),
(13, 'needs', 'needs', 'Need', 'Needs', NULL, 'App\\Models\\Need', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-15 00:50:41', '2018-11-21 07:08:33'),
(14, 'sales', 'sales', 'Sale', 'Sales', NULL, 'App\\Models\\Sale', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-15 00:57:32', '2018-11-15 04:19:46'),
(15, 'hardware_maintenances', 'hardware-maintenances', 'Hardware Maintenance', 'Hardware Maintenances', NULL, 'App\\Models\\HardwareMaintenance', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-15 01:21:59', '2018-11-19 05:16:41'),
(16, 'hardware_registrations', 'hardware-registrations', 'Hardware Registration', 'Hardware Registrations', NULL, 'App\\Models\\HardwareRegistration', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-15 01:34:31', '2018-11-15 01:36:58'),
(17, 'charges', 'charges', 'Charge', 'Charges', NULL, 'App\\Models\\Charge', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-16 00:08:20', '2018-11-16 00:39:16'),
(18, 'lottery_results', 'lottery-results', 'Lottery Result', 'Lottery Results', NULL, 'App\\Models\\LotteryResult', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-19 04:36:30', '2018-11-19 04:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_type_id` int(11) NOT NULL,
  `emp_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('Female','Male') COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wage_type` enum('Monthly','By Monthly') COLLATE utf8mb4_unicode_ci NOT NULL,
  `wage` double(8,2) NOT NULL,
  `marital_status` enum('Married','Single','Divorced','Widowed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `attached_doc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spouse_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spouse_date_of_birth` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `emp_id`, `emp_type_id`, `emp_name`, `gender`, `date_of_birth`, `place_of_birth`, `nationality`, `address`, `job_title`, `wage_type`, `wage`, `marital_status`, `attached_doc`, `spouse_name`, `spouse_date_of_birth`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'test123', 2, 'test', 'Female', '1993-02-03', 'test', 'India', 'test', 'test', 'Monthly', 23.56, 'Single', 'employees\\November2018\\MFxLa5xRYrffuQ34xSc6.jpg', 'test', '1992-03-03', 0, NULL, '2018-11-15 00:42:41', '2018-11-15 00:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `employee_child`
--

CREATE TABLE `employee_child` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child_date_of_birth` date NOT NULL,
  `attached_doc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attending_school` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_payments`
--

CREATE TABLE `employee_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_type_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_types`
--

CREATE TABLE `employee_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_types`
--

INSERT INTO `employee_types` (`id`, `emp_type`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'CPoS', 2, NULL, '2018-11-14 05:11:38', '2018-11-14 05:11:38'),
(3, 'PoS', 2, NULL, '2018-11-14 05:11:48', '2018-11-14 05:11:48'),
(4, 'HPoS', 2, NULL, '2018-11-14 05:11:56', '2018-11-14 05:11:56'),
(5, 'Staff', 2, NULL, '2018-11-14 05:12:03', '2018-11-14 05:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `hardware_maintenances`
--

CREATE TABLE `hardware_maintenances` (
  `id` int(10) UNSIGNED NOT NULL,
  `hw_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_id` int(11) NOT NULL,
  `problem_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `resolve` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolve_date` date DEFAULT NULL,
  `tech_info` text COLLATE utf8mb4_unicode_ci,
  `resolved_by` int(11) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hardware_maintenances`
--

INSERT INTO `hardware_maintenances` (`id`, `hw_id`, `location_id`, `problem_description`, `date`, `resolve`, `resolve_date`, `tech_info`, `resolved_by`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1', 2, 'test', '2018-01-31', NULL, NULL, NULL, NULL, 2, NULL, '2018-11-19 05:23:46', '2018-11-19 05:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `hardware_registrations`
--

CREATE TABLE `hardware_registrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `hw_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `date_of_purchase` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hardware_registrations`
--

INSERT INTO `hardware_registrations` (`id`, `hw_id`, `device`, `location_id`, `description`, `price`, `date_of_purchase`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'test123', '321dfdsaf', 2, 'test', 23.45, '2018-12-31', 2, NULL, '2018-11-15 01:37:26', '2018-11-15 01:37:26');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location_name`, `pos_id`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'test', '1234345', 2, NULL, '2018-11-14 01:40:49', '2018-11-14 01:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `lottery_results`
--

CREATE TABLE `lottery_results` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `lottery_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lottery_results`
--

INSERT INTO `lottery_results` (`id`, `location_id`, `date`, `lottery_number`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, '2018-11-19', '1,2,3,4,5', 2, NULL, '2018-11-19 04:40:43', '2018-11-19 04:40:43');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-11-10 04:57:51', '2018-11-10 04:57:51');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2018-11-10 04:57:51', '2018-11-10 04:57:51', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2018-11-10 04:57:51', '2018-11-10 04:57:51', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2018-11-10 04:58:01', '2018-11-10 04:58:01', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2018-11-10 04:58:03', '2018-11-10 04:58:03', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2018-11-10 04:58:05', '2018-11-10 04:58:05', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2018-11-10 04:58:09', '2018-11-10 04:58:09', 'voyager.hooks', NULL),
(15, 1, 'Locations', '', '_self', NULL, NULL, NULL, 15, '2018-11-14 01:37:35', '2018-11-14 01:37:35', 'voyager.locations.index', NULL),
(16, 1, 'Employee Types', '', '_self', NULL, NULL, NULL, 16, '2018-11-14 05:09:21', '2018-11-14 05:09:21', 'voyager.employee-types.index', NULL),
(17, 1, 'Employees', '', '_self', NULL, NULL, NULL, 17, '2018-11-14 23:08:46', '2018-11-14 23:08:46', 'voyager.employees.index', NULL),
(18, 1, 'Needs', '', '_self', NULL, NULL, NULL, 18, '2018-11-15 00:50:41', '2018-11-15 00:50:41', 'voyager.needs.index', NULL),
(19, 1, 'Sales', '', '_self', NULL, NULL, NULL, 19, '2018-11-15 00:57:32', '2018-11-15 00:57:32', 'voyager.sales.index', NULL),
(20, 1, 'Hardware Maintenances', '', '_self', NULL, NULL, NULL, 20, '2018-11-15 01:21:59', '2018-11-15 01:21:59', 'voyager.hardware-maintenances.index', NULL),
(21, 1, 'Hardware Registrations', '', '_self', NULL, NULL, NULL, 21, '2018-11-15 01:34:31', '2018-11-15 01:34:31', 'voyager.hardware-registrations.index', NULL),
(22, 1, 'Charges', '', '_self', NULL, NULL, NULL, 22, '2018-11-16 00:08:21', '2018-11-16 00:08:21', 'voyager.charges.index', NULL),
(23, 1, 'Lottery Results', '', '_self', NULL, NULL, NULL, 23, '2018-11-19 04:36:31', '2018-11-19 04:36:31', 'voyager.lottery-results.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(36, '2016_01_01_000000_create_pages_table', 2),
(37, '2016_01_01_000000_create_posts_table', 2),
(38, '2016_02_15_204651_create_categories_table', 2),
(39, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(40, '2017_11_26_015000_create_user_roles_table', 2),
(41, '2018_03_11_000000_add_user_settings', 2),
(42, '2018_03_14_000000_add_details_to_data_types_table', 2),
(46, '2018_03_16_000000_make_settings_value_nullable', 3),
(49, '2018_11_12_062146_employee', 4),
(50, '2018_11_12_062933_employee_child', 5),
(51, '2018_11_12_063354_location', 6),
(52, '2018_11_12_063546_employee_type', 7),
(53, '2018_11_12_063721_needs', 8),
(54, '2018_11_12_063922_sell', 9),
(55, '2018_11_12_064020_charges', 10),
(56, '2018_11_12_064251_charges_transaction', 11),
(57, '2018_11_12_064447_employee_payment', 12),
(58, '2018_11_12_064713_hardware_registration', 13),
(59, '2018_11_12_064950_hardware_maintenance', 14),
(60, '2018_11_19_100152_lottery_results', 15);

-- --------------------------------------------------------

--
-- Table structure for table `needs`
--

CREATE TABLE `needs` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `needs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `date` date NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` enum('approve','serve','pending') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `needs`
--

INSERT INTO `needs` (`id`, `emp_id`, `location_id`, `needs`, `price`, `date`, `comment`, `added_by`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'coffee machine', 23.45, '2018-01-01', 'test', 2, 'approve', NULL, '2018-11-15 01:17:39', '2018-11-15 23:54:12'),
(2, 1, 2, 'coffee machine', 23.45, '2018-01-31', 'test', 2, 'approve', NULL, '2018-11-15 23:53:54', '2018-11-15 23:53:54'),
(3, 1, 2, 'cooffee', 23.45, '2018-09-29', 'test', 2, 'approve', NULL, '2018-11-15 23:54:41', '2018-11-15 23:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(2, 'browse_bread', NULL, '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(3, 'browse_database', NULL, '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(4, 'browse_media', NULL, '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(5, 'browse_compass', NULL, '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(6, 'browse_menus', 'menus', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(7, 'read_menus', 'menus', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(8, 'edit_menus', 'menus', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(9, 'add_menus', 'menus', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(10, 'delete_menus', 'menus', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(11, 'browse_roles', 'roles', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(12, 'read_roles', 'roles', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(13, 'edit_roles', 'roles', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(14, 'add_roles', 'roles', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(15, 'delete_roles', 'roles', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(16, 'browse_users', 'users', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(17, 'read_users', 'users', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(18, 'edit_users', 'users', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(19, 'add_users', 'users', '2018-11-10 04:57:52', '2018-11-10 04:57:52'),
(20, 'delete_users', 'users', '2018-11-10 04:57:53', '2018-11-10 04:57:53'),
(21, 'browse_settings', 'settings', '2018-11-10 04:57:53', '2018-11-10 04:57:53'),
(22, 'read_settings', 'settings', '2018-11-10 04:57:53', '2018-11-10 04:57:53'),
(23, 'edit_settings', 'settings', '2018-11-10 04:57:53', '2018-11-10 04:57:53'),
(24, 'add_settings', 'settings', '2018-11-10 04:57:53', '2018-11-10 04:57:53'),
(25, 'delete_settings', 'settings', '2018-11-10 04:57:53', '2018-11-10 04:57:53'),
(26, 'browse_categories', 'categories', '2018-11-10 04:58:01', '2018-11-10 04:58:01'),
(27, 'read_categories', 'categories', '2018-11-10 04:58:01', '2018-11-10 04:58:01'),
(28, 'edit_categories', 'categories', '2018-11-10 04:58:01', '2018-11-10 04:58:01'),
(29, 'add_categories', 'categories', '2018-11-10 04:58:01', '2018-11-10 04:58:01'),
(30, 'delete_categories', 'categories', '2018-11-10 04:58:01', '2018-11-10 04:58:01'),
(31, 'browse_posts', 'posts', '2018-11-10 04:58:03', '2018-11-10 04:58:03'),
(32, 'read_posts', 'posts', '2018-11-10 04:58:03', '2018-11-10 04:58:03'),
(33, 'edit_posts', 'posts', '2018-11-10 04:58:03', '2018-11-10 04:58:03'),
(34, 'add_posts', 'posts', '2018-11-10 04:58:03', '2018-11-10 04:58:03'),
(35, 'delete_posts', 'posts', '2018-11-10 04:58:03', '2018-11-10 04:58:03'),
(36, 'browse_pages', 'pages', '2018-11-10 04:58:05', '2018-11-10 04:58:05'),
(37, 'read_pages', 'pages', '2018-11-10 04:58:05', '2018-11-10 04:58:05'),
(38, 'edit_pages', 'pages', '2018-11-10 04:58:05', '2018-11-10 04:58:05'),
(39, 'add_pages', 'pages', '2018-11-10 04:58:05', '2018-11-10 04:58:05'),
(40, 'delete_pages', 'pages', '2018-11-10 04:58:05', '2018-11-10 04:58:05'),
(41, 'browse_hooks', NULL, '2018-11-10 04:58:09', '2018-11-10 04:58:09'),
(42, 'browse_locations', 'locations', '2018-11-14 01:37:35', '2018-11-14 01:37:35'),
(43, 'read_locations', 'locations', '2018-11-14 01:37:35', '2018-11-14 01:37:35'),
(44, 'edit_locations', 'locations', '2018-11-14 01:37:35', '2018-11-14 01:37:35'),
(45, 'add_locations', 'locations', '2018-11-14 01:37:35', '2018-11-14 01:37:35'),
(46, 'delete_locations', 'locations', '2018-11-14 01:37:35', '2018-11-14 01:37:35'),
(47, 'browse_employee_types', 'employee_types', '2018-11-14 05:09:21', '2018-11-14 05:09:21'),
(48, 'read_employee_types', 'employee_types', '2018-11-14 05:09:21', '2018-11-14 05:09:21'),
(49, 'edit_employee_types', 'employee_types', '2018-11-14 05:09:21', '2018-11-14 05:09:21'),
(50, 'add_employee_types', 'employee_types', '2018-11-14 05:09:21', '2018-11-14 05:09:21'),
(51, 'delete_employee_types', 'employee_types', '2018-11-14 05:09:21', '2018-11-14 05:09:21'),
(52, 'browse_employees', 'employees', '2018-11-14 23:08:45', '2018-11-14 23:08:45'),
(53, 'read_employees', 'employees', '2018-11-14 23:08:45', '2018-11-14 23:08:45'),
(54, 'edit_employees', 'employees', '2018-11-14 23:08:45', '2018-11-14 23:08:45'),
(55, 'add_employees', 'employees', '2018-11-14 23:08:45', '2018-11-14 23:08:45'),
(56, 'delete_employees', 'employees', '2018-11-14 23:08:45', '2018-11-14 23:08:45'),
(57, 'browse_needs', 'needs', '2018-11-15 00:50:41', '2018-11-15 00:50:41'),
(58, 'read_needs', 'needs', '2018-11-15 00:50:41', '2018-11-15 00:50:41'),
(59, 'edit_needs', 'needs', '2018-11-15 00:50:41', '2018-11-15 00:50:41'),
(60, 'add_needs', 'needs', '2018-11-15 00:50:41', '2018-11-15 00:50:41'),
(61, 'delete_needs', 'needs', '2018-11-15 00:50:41', '2018-11-15 00:50:41'),
(62, 'browse_sales', 'sales', '2018-11-15 00:57:32', '2018-11-15 00:57:32'),
(63, 'read_sales', 'sales', '2018-11-15 00:57:32', '2018-11-15 00:57:32'),
(64, 'edit_sales', 'sales', '2018-11-15 00:57:32', '2018-11-15 00:57:32'),
(65, 'add_sales', 'sales', '2018-11-15 00:57:32', '2018-11-15 00:57:32'),
(66, 'delete_sales', 'sales', '2018-11-15 00:57:32', '2018-11-15 00:57:32'),
(67, 'browse_hardware_maintenances', 'hardware_maintenances', '2018-11-15 01:21:59', '2018-11-15 01:21:59'),
(68, 'read_hardware_maintenances', 'hardware_maintenances', '2018-11-15 01:21:59', '2018-11-15 01:21:59'),
(69, 'edit_hardware_maintenances', 'hardware_maintenances', '2018-11-15 01:21:59', '2018-11-15 01:21:59'),
(70, 'add_hardware_maintenances', 'hardware_maintenances', '2018-11-15 01:21:59', '2018-11-15 01:21:59'),
(71, 'delete_hardware_maintenances', 'hardware_maintenances', '2018-11-15 01:21:59', '2018-11-15 01:21:59'),
(72, 'browse_hardware_registrations', 'hardware_registrations', '2018-11-15 01:34:31', '2018-11-15 01:34:31'),
(73, 'read_hardware_registrations', 'hardware_registrations', '2018-11-15 01:34:31', '2018-11-15 01:34:31'),
(74, 'edit_hardware_registrations', 'hardware_registrations', '2018-11-15 01:34:31', '2018-11-15 01:34:31'),
(75, 'add_hardware_registrations', 'hardware_registrations', '2018-11-15 01:34:31', '2018-11-15 01:34:31'),
(76, 'delete_hardware_registrations', 'hardware_registrations', '2018-11-15 01:34:31', '2018-11-15 01:34:31'),
(77, 'browse_charges', 'charges', '2018-11-16 00:08:20', '2018-11-16 00:08:20'),
(78, 'read_charges', 'charges', '2018-11-16 00:08:20', '2018-11-16 00:08:20'),
(79, 'edit_charges', 'charges', '2018-11-16 00:08:20', '2018-11-16 00:08:20'),
(80, 'add_charges', 'charges', '2018-11-16 00:08:20', '2018-11-16 00:08:20'),
(81, 'delete_charges', 'charges', '2018-11-16 00:08:20', '2018-11-16 00:08:20'),
(82, 'browse_lottery_results', 'lottery_results', '2018-11-19 04:36:30', '2018-11-19 04:36:30'),
(83, 'read_lottery_results', 'lottery_results', '2018-11-19 04:36:30', '2018-11-19 04:36:30'),
(84, 'edit_lottery_results', 'lottery_results', '2018-11-19 04:36:30', '2018-11-19 04:36:30'),
(85, 'add_lottery_results', 'lottery_results', '2018-11-19 04:36:31', '2018-11-19 04:36:31'),
(86, 'delete_lottery_results', 'lottery_results', '2018-11-19 04:36:31', '2018-11-19 04:36:31');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(1, 5),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(42, 3),
(42, 5),
(43, 1),
(43, 3),
(43, 5),
(44, 1),
(44, 3),
(44, 5),
(45, 1),
(45, 3),
(45, 5),
(46, 1),
(46, 3),
(46, 5),
(47, 1),
(47, 3),
(47, 5),
(48, 1),
(48, 3),
(48, 5),
(49, 1),
(49, 3),
(49, 5),
(50, 1),
(50, 3),
(50, 5),
(51, 1),
(51, 3),
(51, 5),
(52, 1),
(52, 3),
(52, 5),
(53, 1),
(53, 3),
(53, 5),
(54, 1),
(54, 3),
(54, 5),
(55, 1),
(55, 3),
(55, 5),
(56, 1),
(56, 3),
(56, 5),
(57, 1),
(57, 3),
(57, 5),
(58, 1),
(58, 3),
(58, 5),
(59, 1),
(59, 3),
(59, 5),
(60, 1),
(60, 3),
(60, 5),
(61, 1),
(61, 3),
(61, 5),
(62, 1),
(62, 3),
(62, 5),
(63, 1),
(63, 3),
(63, 5),
(64, 1),
(64, 3),
(64, 5),
(65, 1),
(65, 3),
(65, 5),
(66, 1),
(66, 3),
(66, 5),
(67, 1),
(67, 3),
(67, 5),
(68, 1),
(68, 3),
(68, 5),
(69, 1),
(69, 3),
(69, 5),
(70, 1),
(70, 3),
(70, 5),
(71, 1),
(71, 3),
(71, 5),
(72, 1),
(72, 3),
(72, 5),
(73, 1),
(73, 3),
(73, 5),
(74, 1),
(74, 3),
(74, 5),
(75, 1),
(75, 3),
(75, 5),
(76, 1),
(76, 3),
(76, 5),
(77, 1),
(77, 3),
(77, 5),
(78, 1),
(78, 3),
(78, 5),
(79, 3),
(79, 5),
(80, 1),
(80, 3),
(80, 5),
(81, 3),
(81, 5),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-11-10 04:57:51', '2018-11-10 04:57:51'),
(2, 'user', 'Normal User', '2018-11-10 04:57:51', '2018-11-10 04:57:51'),
(3, 'Super Admin', 'Super Admin', '2018-11-16 08:03:52', '2018-11-16 08:06:11'),
(5, 'Accounting Admin', 'Admin', '2018-11-16 08:06:48', '2018-11-16 08:06:48');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `date` date NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `emp_id`, `location_id`, `price`, `date`, `comment`, `added_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 23.45, '2015-01-01', 'test', 2, NULL, '2018-11-15 01:17:12', '2018-11-15 23:55:17');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Accounting', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-11-10 04:58:05', '2018-11-10 04:58:05'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-11-10 04:58:05', '2018-11-10 04:58:05'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-11-10 04:58:06', '2018-11-10 04:58:06'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-11-10 04:58:07', '2018-11-10 04:58:07'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-11-10 04:58:07', '2018-11-10 04:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', '$2y$10$7KqZuwpfdQloqEoDZ1czZ.8M1htw4QlWRGIlx/4utPGLOSPiE997e', 'DzA7cW1RntCtZGdBZgokcHSxYW5b41qWdHFYuTtIcLxQU4rR1yW1yNG2gOxQ', NULL, '2018-11-10 04:58:02', '2018-11-10 04:58:02'),
(2, 1, 'admin', 'admin@gmail.com', 'users/November2018/PPzVexFFZjYPEtmBpZPe.png', '$2y$10$5nRxgTjpdNp6e4BPomrvwOnBpTdcS2EdSl30.tQbxsTA8aIU.C8NG', '0bTgR74kPR8NYeW5Re9HvWTKkveS8HZnLMfHdy4EEkqnqhA1bSXhq8F6H4eF', '{\"locale\":\"en\"}', '2018-11-10 04:58:43', '2018-11-20 09:56:55'),
(3, 3, 'Marcus Superadmin', 'superadmin@gmail.com', 'users/November2018/qjCKa6RGDb3TBF4LR7W5.png', '$2y$10$Awize3nCEZKma02kUCH10udKSTIqWd1FZ1uD6a/RBfYaVglUi.Esm', '8U3DpSeo9rgBcTgwyoGWdF4TXy0JKYv29i2FXQdk6948NIENprZIFzTKfIgm', '{\"locale\":\"en\"}', '2018-11-16 08:07:44', '2018-11-20 11:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(3, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `charges`
--
ALTER TABLE `charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `charge_transactions`
--
ALTER TABLE `charge_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_child`
--
ALTER TABLE `employee_child`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_payments`
--
ALTER TABLE `employee_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_types`
--
ALTER TABLE `employee_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hardware_maintenances`
--
ALTER TABLE `hardware_maintenances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hardware_registrations`
--
ALTER TABLE `hardware_registrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lottery_results`
--
ALTER TABLE `lottery_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `needs`
--
ALTER TABLE `needs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `charges`
--
ALTER TABLE `charges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `charge_transactions`
--
ALTER TABLE `charge_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_child`
--
ALTER TABLE `employee_child`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_payments`
--
ALTER TABLE `employee_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_types`
--
ALTER TABLE `employee_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hardware_maintenances`
--
ALTER TABLE `hardware_maintenances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hardware_registrations`
--
ALTER TABLE `hardware_registrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lottery_results`
--
ALTER TABLE `lottery_results`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `needs`
--
ALTER TABLE `needs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
