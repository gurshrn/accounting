<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Sale extends Model
{
     public function save(array $options = [])
    {
        $this->added_by = Auth::user()->id;
        parent::save();
    }
}
