<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LotteryResult extends Model
{
    public function save(array $options = [])
    {
        $this->added_by = Auth::user()->id;
        parent::save();
    }
    public function location()
    {
    	return $this->belongsTo('App\Models\Location','location_id');
    }
}
