<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Charge extends Model
{
    public function transaction()
    {
    	/*charges_id = foreign key*/
    	return $this->hasMany('App\Models\ChargeTransaction','charges_id');
    }
    public function employee()
    {
    	return $this->belongsTo('App\Models\Employee','emp_id');
    }
    public function location()
    {
    	return $this->belongsTo('App\Models\Location','location_id');
    }
}
