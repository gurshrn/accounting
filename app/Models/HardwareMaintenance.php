<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class HardwareMaintenance extends Model
{
    public function save(array $options = [])
    {
        $this->added_by = Auth::user()->id;
        parent::save();
    }
    public function hardware()
    {
    	return $this->belongsTo('App\Models\HardwareRegistration','hw_id');
    }

}
