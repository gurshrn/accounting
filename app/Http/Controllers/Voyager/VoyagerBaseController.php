<?php

namespace App\Http\Controllers\Voyager;
use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\Charge;
use App\Models\ChargeTransaction;
use App\Models\HardwareMaintenance;
use App\Models\Location;
use App\Models\EmployeeType;
use App\Models\WorkingSchedule;
use App\Models\Employee;
use App\Models\WorkingScheduleAdjustment;
use Illuminate\Support\Facades\Auth;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class VoyagerBaseController extends BaseVoyagerBaseController
{
	public function login()
	{
		return view('vendor.voyager.login.edit-add');
	}
    public function addChargeTransaction(Request $request)
    {
    	$charge = new Charge;
    	$charge->emp_id = $request->input('emp_id');
        $charge->location_id = $request->input('location_id');
        $charge->needs_type = $request->input('needs_type');
        $charge->total_amount = $request->input('total_amount');
        $charge->paid_amount = $request->input('paid_amount');
        $charge->comment = $request->input('comment');
        $charge->date = $request->input('date');
        $charge->added_by = Auth::user()->id;
        $charge->save();

        $chargeTrans = new ChargeTransaction;
    	$chargeTrans->charges_id = $charge->id;
    	$chargeTrans->paid_amount = $request->input('paid_amount');
    	$chargeTrans->date = $request->input('date');
		$chargeTrans->comment=$request->input('comment');
		$chargeTrans->save();
		return redirect()->back();
    }
    public function viewTransactions($id)
    {
    	$charge = Charge::where('id', '=', $id)->first();
    	return view('charges.charges-transaction', ['charge'=>$charge]);
    }
    public function addTransaction(Request $request)
    {
    	$transaction = new ChargeTransaction;

    	$transaction->charges_id = $request->input('charges_id');
    	$transaction->date = $request->input('date');
        $transaction->paid_amount = $request->input('amount');
        $transaction->comment = $request->input('comment');
       	$transaction->save();
       	return redirect()->back();
    }
    public function hardwareResolve($id)
    {
    	$hardwareResolve = HardwareMaintenance::where('hw_id', '=', $id)->first();
    	return view('hardware-maintenance.maintenance-resolve', ['hardwareMaintenance'=>$hardwareResolve]);
    }
    public function resolveHardwareProblem(Request $request)
    {
    	$hardware = new HardwareMaintenance;
    	$maintenanceId = $request->input('maintenance_id');

    	$hardware = HardwareMaintenance::where('id', $maintenanceId)->first();
    	
    	$hardware->resolve_date = $request->input('resolve_date');
    	$hardware->resolve_cost = $request->input('resolve_cost');
        $hardware->resolved_by = Auth::user()->id;
        $hardware->tech_info = $request->input('technical_info');
        $hardware->resolve_date = date('Y-m-d');
        $hardware->resolve = 'Yes';
        $hardware->save();
       	
       	return redirect()->back();
    }

    /*==================Show Working Schedule List====================*/

    public function workingScheduleList()
    {
    	return view('vendor.voyager.working-schedules.browse');
    }

    /*==================Add and esit view of working schedule====================*/

    public function workingSchedule()
    {
    	$data['location'] = Location::get();
    	$data['empType'] = EmployeeType::get();
    	$data['employee'] = Employee::get();
    	return view('vendor.voyager.working-schedules.edit-add',['data'=>$data]);
    }

    /*==================Save working Schedule list====================*/
   
	public function addWorkingSchedule(Request $request)
    {
    	$WorkingSchedule = new WorkingSchedule;

    	$WorkingSchedule->emp_type_id = $request->input('emp_type_id');
    	$WorkingSchedule->emp_id = $request->input('emp_id');
    	$WorkingSchedule->location_id = $request->input('location_id');
    	$WorkingSchedule->comment = $request->input('comment');
    	$WorkingSchedule->added_by = Auth::user()->id;
    	$WorkingSchedule->working_days = implode(",",$request->input('working_days'));
    	
    	$WorkingSchedule->save();

    	

    	foreach($request->input('group') as $val)
    	{
    		$WorkingScheduleAdjustment = new WorkingScheduleAdjustment();
			$WorkingScheduleAdjustment->work_schedule_id = $WorkingSchedule->id;
	    	$WorkingScheduleAdjustment->emp_id = $val['emp_id'];
	    	$WorkingScheduleAdjustment->days_count = $val['days_count'];
			$WorkingScheduleAdjustment->week_days=$val['week_days'];
			$WorkingScheduleAdjustment->added_by = Auth::user()->id;
			$WorkingScheduleAdjustment->save();
    	}
    	
		return redirect()->back();
    }
    public function getEmployeesByEmpId(Request $request)
    {
    	$empTypeId = $request->input('empTypeId');
    	$employee = Employee::where('emp_type_id', $empTypeId)->get();
    	$data = '';
    	$data .= '<option value="">--None--</option>';
    	if(!empty($employee))
    	{
    		foreach($employee as $val)
	    	{
	    		$data .= '<option value="'.$val->id.'">'.$val->emp_name.'</option>';
	    	}
    	}
    	echo $data;
    }


}
