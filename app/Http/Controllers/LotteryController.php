<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\LotteryResult;

class LotteryController extends Controller
{
    public function lottery()
    {
    	$lotteryResults = LotteryResult::orderby('created_at','DESC')->get();
    	return view('frontend.index',['lotteryResults'=>$lotteryResults]);
    }
    public function searchLottery(Request $request)
    {
    	if($request->input('lotteryDate') != '')
    	{
    		$lotteryResults = LotteryResult::where('date',date('Y-m-d',strtotime($request->input('lotteryDate'))))->orderby('created_at','DESC')->get();
    	}
    	else
    	{
    		$lotteryResults = LotteryResult::orderby('created_at','DESC')->get();
    	}
    	
    	return view('frontend.search-lottery-results',['lotteryResults'=>$lotteryResults]);
    }
}
