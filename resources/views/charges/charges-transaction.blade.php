@php $employeeDetail = $charge->employee;
$chargeDetail = $charge;
$locationDetail = $charge->location;
$transaction = $charge->transaction;
$totalPendingAmt = $chargeDetail->transaction->sum('paid_amount');


@endphp
@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    

                        <div class="panel-body">
                        	<div class="panel-heading" style="border-bottom:0;">
                            	<h3 class="panel-title">Employee</h3>
                            	<div class="panel-body" style="padding-top:0;">
                                	<p>{{ucfirst($employeeDetail->emp_name)}}</p>
            					</div>
                            	<hr style="margin:0;">
	                            <div class="panel-heading" style="border-bottom:0;">
	                            	<h3 class="panel-title">Location</h3>
	                        	</div>

	                       		<div class="panel-body" style="padding-top:0;">
	                                <p>{{ucfirst($locationDetail->location_name)}}</p>
	                			</div><!-- panel-body -->
	                            <hr style="margin:0;">
	                            <div class="panel-heading" style="border-bottom:0;">
	                            	<h3 class="panel-title">PoS Id</h3>
	                        	</div>

	                       		<div class="panel-body" style="padding-top:0;">
	                                <p>{{$locationDetail->pos_id}}</p>
	                			</div><!-- panel-body -->
	                			<hr style="margin:0;">
	                			<div class="panel-heading" style="border-bottom:0;">
	                            	<h3 class="panel-title">Comment</h3>
	                        	</div>

	                       		<div class="panel-body" style="padding-top:0;">
	                                <p>{{ucfirst($chargeDetail->comment)}}</p>
	                			</div><!-- panel-body -->
	                			<hr style="margin:0;">
	                			<div class="panel-heading" style="border-bottom:0;">
	                            	<h3 class="panel-title">Total Amount</h3>
	                        	</div>

	                       		<div class="panel-body" style="padding-top:0;">
	                                <p>{{$chargeDetail->total_amount}}</p>
	                			</div><!-- panel-body -->
	                			<hr style="margin:0;">
	                			<div class="panel-heading" style="border-bottom:0;">
	                            	<h3 class="panel-title">Total Paid Amount</h3>
	                        	</div>

	                       		<div class="panel-body" style="padding-top:0;">
	                                <p>{{$chargeDetail->transaction->sum('paid_amount')}}</p>
	                            </div>
	                                <hr style="margin:0;">
	                                <div class="panel-heading" style="border-bottom:0;">
	                            	<h3 class="panel-title">Pending Amount</h3>
	                        	</div>

	                       		<div class="panel-body" style="padding-top:0;">
	                                <p>{{$chargeDetail->total_amount-$chargeDetail->transaction->sum('paid_amount')}}</p>
	                            </div>
	                                <hr style="margin:0;">
	                			<div class="panel-heading" style="border-bottom:0;">
	                				
	                				
	                            	<h3 class="panel-title">Status</h3>
	                        	</div>

	                       		<div class="panel-body" style="padding-top:0;">
	                                <p>
	                                	@if($chargeDetail->total_amount == $totalPendingAmt)
	                						{{'Fully Paid'}}
	                					@else
	                						{{'Pending'}}
	                				@endif
	                				</p>
	                			</div>
							</div>
                        </div>
                        	
                        <div class="panel-body">
                          	<table class="table table-bordered">
							  	<thead>
								    <tr>
								      <th scope="col">Date</th>
								      <th scope="col">Comment</th>
								      <th scope="col">Price</th>
								    </tr>
							  	</thead>
							  	<tbody>
							  		@foreach ($transaction as $val) 

									    <tr>
									      <th scope="row">{{date('m/d/Y',strtotime($val->created_at))}}</th>
									      <td>{{ucfirst($val->comment)}}</td>
									      <td>{{$val->paid_amount}}</td>
									    </tr>

								    @endforeach
							  	</tbody>
							</table>           
                        </div>
                        @if($chargeDetail->total_amount != $totalPendingAmt)
 
	                        <div class="panel-body">
		                        <form role="form" action="{{route('voyager.charges-transaction.store')}}" method="POST">
	                        		{{ csrf_field() }}

	                        		<input type="hidden" name="charges_id" value="{{$chargeDetail->id}}">
	                        		<input type="hidden" name="total_amount" value="{{$chargeDetail->total_amount}}">

	                        		<div class="form-group">
									    <label for="formGroupExampleInput">Date</label>
									    <input type="date" class="form-control" name="date" id="formGroupExampleInput" value="{{date('Y-m-d')}}"  placeholder="Date">
								  	</div>

								  	<div class="form-group">
									    <label for="formGroupExampleInput">Amount</label>
									    <input type="text" class="form-control" name="amount" id="formGroupExampleInput" value="{{$chargeDetail->total_amount-$chargeDetail->transaction->sum('paid_amount')}}" placeholder="Amount">
								  	</div>
								  	<div class="form-group">
									    <label for="formGroupExampleInput2">Comment</label>
									    <input type="text" class="form-control" name="comment" id="formGroupExampleInput2" placeholder="Comment">
								  	</div>
								  	<div class="panel-footer">
		                            	<button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
		                        	</div>
								</form>
							</div>
						@endif
                 
					<iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

