@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                            <div class="form-group  col-md-12">
                                <label for="name">Employee Id</label>
                                    <input required="" type="text" class="form-control" name="emp_id" placeholder="Employee Id" value="">
                            </div>
                            <div class="form-group  col-md-12">
                                        
                                <label for="name">Employee Type</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="emp_type_id" tabindex="-1" aria-hidden="true">
                    
                                        <option value="">None</option>
                                        <option value="2">CPoS</option>
                                        <option value="3">PoS</option>
                                        <option value="4">HPoS</option>
                                        <option value="5">Staff</option>

                                    </select>
                            </div>

                            <div class="form-group  col-md-12">

                                <label for="name">Employee Name</label>
                                    <input required="" type="text" class="form-control" name="emp_name" placeholder="Employee Name" value="">

                            </div>

                            <div class="form-group  col-md-12">
                                <label for="name">Gender</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="gender" tabindex="-1" aria-hidden="true">
                                        <option value="_empty_">-- None --</option>
                                        <option value="Female">Female</option>
                                        <option value="Male">Male</option>
                                     </select>

                                    </div>
                                   <div class="form-group  col-md-12">

                                        <label for="name">Date Of Birth</label>
                                            <input type="date" class="form-control" name="date_of_birth" placeholder="Date Of Birth" value="">
                                    </div>

                                    <div class="form-group  col-md-12">
                                        
                                        <label for="name">Place Of Birth</label>
                                            <input required="" type="text" class="form-control" name="place_of_birth" placeholder="Place Of Birth" value="">
                                    </div>

                                    <div class="form-group  col-md-12">
                                        
                                        <label for="name">Nationality</label>
                                            <input required="" type="text" class="form-control" name="nationality" placeholder="Nationality" value="">
                                    </div>

                                    <div class="form-group  col-md-12">
                                        
                                        <label for="name">Address</label>
                                            <input required="" type="text" class="form-control" name="address" placeholder="Address" value="">
                                    </div>                                            <div class="form-group  col-md-12">
                                        
                                        <label for="name">Job Title</label>
                                            <input required="" type="text" class="form-control" name="job_title" placeholder="Job Title" value="">
                                    </div>

                                    <div class="form-group  col-md-12">
                                        
                                        <label for="name">Wage Type</label>
                                            <ul class="radio">
                                                <li>
                                                    <input type="radio" id="option-wage-type-monthly" name="wage_type" value="Monthly">
                                                        <label for="option-wage-type-monthly">Monthly</label>
                                                        <div class="check"></div>
                                                </li>
                                                <li>
                                                    <input type="radio" id="option-wage-type-by-monthly" name="wage_type" value="By Monthly">
                                                    <label for="option-wage-type-by-monthly">By Monthly</label>
                                                    <div class="check"></div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="form-group  col-md-12">
                                        
                                            <label for="name">Wage</label>
                                                <input required="" type="text" class="form-control" name="wage" placeholder="Wage" value="">
                                        </div>
                                     <!-- GET THE DISPLAY OPTIONS -->
                                        <div class="form-group  col-md-12">
                                        
                                            <label for="name">Marital Status</label>
                                                <select class="form-control select2 select2-hidden-accessible" name="marital_status" tabindex="-1" aria-hidden="true">
                                                    <option value="_empty_">-- None --</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Single">Single</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Widowed">Widowed</option>
                                                </select>
                                        </div>

                                        <div class="form-group  col-md-12">
                                        
                                        <label for="name">Attached Document</label>
                                            <input required="" type="file" name="attached_doc" accept="image/*">
                                        </div>
                       <!-- GET THE DISPLAY OPTIONS -->
                                        <div class="form-group  col-md-12">
                                        
                                            <label for="name">Spouse Name</label>
                                                <input required="" type="text" class="form-control" name="spouse_name" placeholder="Spouse Name" value="">
                                        </div>
                        <!-- GET THE DISPLAY OPTIONS -->
                                        <div class="form-group  col-md-12">
                                        
                                            <label for="name">Spouse Date Of Birth</label>
                                                <input type="date" class="form-control" name="spouse_date_of_birth" placeholder="Spouse Date Of Birth" value="">
                                        </div>
                                                            
                                    </div>
                 


                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $image;

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
