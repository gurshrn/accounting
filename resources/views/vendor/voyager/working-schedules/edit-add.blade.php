@php
$location = $data['location'];
$empType = $data['empType'];
$employee = $data['employee'];
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop



@section('page_header')
    <h1 class="page-title">
        <i class=""></i>
       Add Working Schedule
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" action="{{route('voyager.add-working-schedule.store')}}" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                          	<div class="form-group  col-md-12">
                                        
                                <label for="name">Employee Type</label>
                                    <select class="form-control select2 select2-hidden-accessible employeeType" name="emp_type_id" tabindex="-1" aria-hidden="true">
                    					<option value="">--None--</option>
                                        @foreach($empType as $type)
											<option value={{$type->id}}>{{ucfirst($type->emp_type)}}</option>
										@endforeach

                                    </select>
                            </div>

                            <div class="form-group  col-md-12">

                                <label for="name">Employee</label>
                               	<select class="form-control select2 select2-hidden-accessible employeeName" name="emp_id" tabindex="-1" aria-hidden="true">
                           			<option value="">--None--</option>
                               	</select>
                            </div>

                            <div class="form-group  col-md-12 locationName">
                               	<label for="name">Location</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="location_id" tabindex="-1" aria-hidden="true">
                    					<option value="">--None--</option>
                                        @foreach($location as $val)
											<option value={{$val->id}}>{{ucfirst($val->location_name)}}</option>
										@endforeach

                                    </select>

                            </div>
                                   
							<div class="form-group  col-md-12">
                                
                                <label for="name">Working Days</label>
                                    <ul class="radio">
                                        <li>
                                            <input type="checkbox" id="Sunday" value="sunday" name="working_days[]">
                                                <label for="Sunday">Sunday</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="Monday" value="monday" name="working_days[]">
                                            <label for="Monday">Monday</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="Tuesday" value="tuesday" name="working_days[]">
                                            <label for="Tuesday">Tuesday</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="Wednesday" value="wednesday" name="working_days[]">
                                            <label for="Wednesday">Wednesday</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="Thursday" value="thursday" name="working_days[]">
                                            <label for="Thursday">Thursday</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="Fiday" value="friday" name="working_days[]">
                                            <label for="Fiday">Fiday</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="Saturday" value="saturday" name="working_days[]">
                                            <label for="Saturday">Saturday</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-group  col-md-12">
	                               	<label for="name">Comment</label>
	                                    <textarea name="comment"></textarea>

	                            </div>
							</div>
							
							<div class="repeater">
								<input data-repeater-create type="button" value="Add Adjustment"/>
								
								<div class="panel-body" data-repeater-list="group">
									<div data-repeater-item>
										<div class="form-group  col-md-12">

			                                <label for="name">Employee</label>
			                               	<select class="form-control" name="emp_id">
			                           			<option value="">--None--</option>
			                           			@foreach($employee as $emp)
			                           				<option value="{{$emp->id}}">{{$emp->emp_name}}</option>
			                           			@endforeach
			                               	</select>
			                            
			                            </div>

			                            <div class="form-group  col-md-12">

			                                <label for="name">Days Count</label>
			                               	<input class="form-control" name="days_count" placeholder="Days Count">
			                           			
			                            </div>

			                            <div class="form-group  col-md-12">

			                                <label for="name">Week Days</label>
			                               	<input class="form-control" name="week_days" placeholder="Week Days">
			                           			
			                            </div>
			                        </div>

								</div>
							</div>
                 


                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
	<script type="text/javascript" src="{{ URL::asset('plugin/repeaters/jquery.repeater.js') }}"></script>
    <script>
    	var repeater = $('.repeater').repeater();

    	$(document).on('change','.employeeType',function(){
    		var empTypeId = $(this).val();
    		if(empTypeId == 2 || empTypeId == 4 || empTypeId == 5)
    		{
    			$('.locationName').css('display','none');
    		}
    		else
    		{
    			$('.locationName').css('display','block');
    		}
    		jQuery.ajax({
    			method: "POST",			
				url: "{{route('voyager.getEmpByEmpId.view')}}",			
				dataType: "html",
				data:{'empTypeId':empTypeId},			
				success: function (data)			
				{				
					$('.employeeName').html(data);
				}		
			});
    		
    	});

        var params = {};
        var $image;

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

           

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
