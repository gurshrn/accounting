@include('frontend/include/header') 
    <!-- header -->
    
    <div class="main">
        <div class="container">
            <section class="form-wrapper">
                <div class="sec-title">
                    <h2>Login</h2>
                </div>
                <!-- sec-title -->
                <form>
                    <div class="form-group">
                        <input type="email" placeholder="Enter You Email" />
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" />
                    </div>
                    <button type="submit" value="submit">login</button>
                    <div class="forgot-pass">
                        <a href="forgot-password.html">
                            Forgot Password?
                        </a>
                    </div>
                </form>
            </section>
            <!-- form-wrap -->
        </div>
        <!-- container -->
    </div>
    <!-- main -->
       
 @include('frontend/include/footer')    

    
</body>
</html>