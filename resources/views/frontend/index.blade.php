@php 
$lotteryResults = $lotteryResults;

@endphp

@include('frontend/include/header') 


        <!-- header -->
        <div class="main">
            <section class="result-wrapper">
                <div class="container">
                    <div class="sec-title">
                        <h2>REsult of lottery</h2>
                    </div>  
                    <!-- sec-title -->
                    <form>
                        <div class="form-group">
                            <figure>
                                <img src="{{asset('/frontend-asset/images/calendar.png')}}" alt="icon" />
                            </figure>
                            <input type="text" id="datepicker" class="lottery-date" placeholder="Date" />
                        </div>
                        {{ csrf_field() }}
                        <button type="button" value="button" id="search-lottery">Search</button>
                    </form>
                    <div id="search-results">
                        <div class="row">
                            @foreach($lotteryResults as $val)
                                @php $explodeLottery = explode(",",$val->lottery_number); @endphp
                                <div class="col-lg-3 col-md-4 col-12">
                                    <div class="grid_item">
                                        <div class="title">
                                            <h3>{{ucfirst($val->location->location_name)}}</h3>
                                        </div>
                                        <ul>
                                            @foreach($explodeLottery as $vals)
                                                <li>{{$vals}}</li>
                                            @endforeach
                                         </ul>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
            </section>
            <!-- result-wrapper -->
        </div>
        <!-- main -->


@include('frontend/include/footer')
<script>
    $(document).on('click','#search-lottery',function(){
        var lotteryDate = $('.lottery-date').val();
        var _token = $("input[name='_token']").val();
        jQuery.ajax({
            method: "POST",          
            url: "{{route('searchLottery')}}",          
            dataType: "html",
            data:{ '_token':_token,'lotteryDate':lotteryDate},           
            success: function (data)            
            {
                          
                $('#search-results').html(data);
                setTimeout(
                    jQuery(window).on("resize scroll", function(e) {
                    var Win = $(window).height();
                    var Header = $("header").height();
                    var Footer = $("footer").height();
                    var NHF = Header + Footer;
                    jQuery('.main').css('min-height', (Win - NHF));
                
                })

                    ,10);
                

            }       
        });
    });

</script> 
    

    
</body>
</html>