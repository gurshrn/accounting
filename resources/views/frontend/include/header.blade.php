<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Accounting Website | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.png" sizes="32x32" type="image/x-icon">

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('/frontend-asset/css/main.css')}}">
</head>

<body>
<div class="wrapper" style="background-image: url({{asset('/frontend-asset/images/background-img.jpg')}})">
        <header class="header-wrapper">
            <div class="container">
                <div class="logo-wrap">
                    <a href="index.html">
                        <img src="{{asset('/frontend-asset/images/logo.png')}}" alt="logo" />
                    </a>
                </div>
                <div class="login-wrap">
                    <a href="{{route('voyager.login')}}">
                        login
                    </a>
                </div>
            </div>
            <!-- container -->
        </header>