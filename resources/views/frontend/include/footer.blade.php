 <footer class="footer-wrapper">
            <div class="container">
                <div class="copyright-sec">
                    <span>Copyright &copy; 2018 Accounting Website. All Rights Reserved.</span>
                </div>
                <div class="poweredby">
                    <span>Powered By - <a href="https://www.imarkinfotech.com/" target="blank">iMark Infotech</a></span>
                </div>
                <!-- poweredby -->
            </div><!-- container -->
        </footer>
        <!-- footer-wrapper -->
    </div>
    <!-- wrapper -->

    <script src="{{asset('/frontend-asset/js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('/frontend-asset/js/jquery.mCustomScrollbar.js')}}"></script>
    <script src="{{asset('/frontend-asset/js/popper.min.js')}}"></script>
    <script src="{{asset('/frontend-asset/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/frontend-asset/js/custom.js')}}"></script>
    <script src="{{asset('/frontend-asset/js/lottery.js')}}"></script>