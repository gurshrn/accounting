<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Accounting Website | Reset Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.png" sizes="32x32" type="image/x-icon">

    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
    <div class="wrapper" style="background-image: url(assets/images/background-img.jpg);">
        <header class="header-wrapper">
            <div class="container">
                <div class="logo-wrap">
                    <a href="index.html">
                        <img src="assets/images/logo.png" alt="logo" />
                    </a>
                </div>
                <div class="login-wrap">
                    <a href="login.html">
                        login
                    </a>
                </div>
            </div>
            <!-- container -->
        </header>
        <!-- header -->
        <div class="main">
            <div class="container">
                <section class="form-wrapper">
                    <div class="sec-title">
                        <h2>Reset Password</h2>
                    </div>
                    <!-- sec-title -->
                    <form>
                        <div class="form-group">
                            <input type="email" placeholder="Enter You Email" />
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Enter New Password" />
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Enter Confirm Password" />
                        </div>
                        <button type="submit" value="submit">Submit</button>
                    </form>
                </section>
                <!-- form-wrap -->
            </div>
            <!-- container -->
        </div>
        <!-- main -->
        <footer class="footer-wrapper">
            <div class="container">
                <div class="copyright-sec">
                    <span>Copyright &copy; 2018 Accounting Website. All Rights Reserved.</span>
                </div>
                <div class="poweredby">
                    <span>Powered By - <a href="https://www.imarkinfotech.com/" target="blank">iMark Infotech</a></span>
                </div>
                <!-- poweredby -->
            </div><!-- container -->
        </footer>
        <!-- footer-wrapper -->
    </div>
    <!-- wrapper -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custom.js"></script>
    

    
</body>
</html>