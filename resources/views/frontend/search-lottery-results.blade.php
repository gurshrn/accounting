<div class="row">@foreach($lotteryResults as $val)
    @php $explodeLottery = explode(",",$val->lottery_number); @endphp
        <div class="col-lg-3 col-md-4 col-12">
            <div class="grid_item">
                <div class="title">
                    <h3>{{ucfirst($val->location->location_name)}}</h3>
                </div>
                <ul>
                    @foreach($explodeLottery as $vals)
                        <li>{{$vals}}</li>
                    @endforeach
                 </ul>
            </div>
        </div>
    @endforeach
</div>


