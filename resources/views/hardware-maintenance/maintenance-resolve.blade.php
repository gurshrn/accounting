@php 

$hardwareMaintenanceData = $hardwareMaintenance;
$hardware = $hardwareMaintenance->hardware;
@endphp
@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    

                    <div class="panel-body">
                       	<div class="panel-heading" style="border-bottom:0;">
                        	<h3 class="panel-title">Hardware Id</h3>
                        	<div class="panel-body" style="padding-top:0;">
                            	<p>{{$hardware->hw_id}}</p>
        					</div>
                        	<hr style="margin:0;">
                        </div>
                        <div class="panel-heading" style="border-bottom:0;">
                        	<h3 class="panel-title">Device Name</h3>
                        	<div class="panel-body" style="padding-top:0;">
                            	<p>{{$hardware->device}}</p>
        					</div>
                        	<hr style="margin:0;">
                        </div>
                        <div class="panel-heading" style="border-bottom:0;">
                        	<h3 class="panel-title">Date</h3>
                        	<div class="panel-body" style="padding-top:0;">
                            	<p>{{date('m/d/Y',strtotime($hardware->created_at))}}</p>
        					</div>
                        	<hr style="margin:0;">
                        </div>
                        <div class="panel-heading" style="border-bottom:0;">
                        	<h3 class="panel-title">Problem Description</h3>
                        	<div class="panel-body" style="padding-top:0;">
                            	<p>{{ucfirst($hardware->description)}}</p>
        					</div>
                        	<hr style="margin:0;">
                        </div>
					</div>
					<div class="panel-body">
						<form role="form" action="{{route('voyager.resolved-hardware-problem.store')}}" method="POST">
                    		{{ csrf_field() }}
                    		<input type="hidden" name="maintenance_id" value="{{$hardwareMaintenanceData->id}}">
							<div class="form-group  col-md-12">
						      	<label for="name">Resolve Date</label>
					         	<input type="date" class="form-control" name="resolve_date" placeholder="Employee Id">
						 	</div>
						 	<div class="form-group  col-md-12">
						      	<label for="name">Cost</label>
					         	<input type="number" class="form-control" name="resolve_cost" placeholder="Cost">
						 	</div>
						 	<div class="form-group  col-md-12">
						      	<label for="name">Technical Information</label>
					         	<textarea class="form-control" name="technical_info" placeholder="Technical Information"></textarea>
						 	</div>
						 	<div class="panel-footer">
								<button type="submit" class="btn btn-primary save">Save</button>
							</div>
						</form>
					</div>

                </div>

                        	
                <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

