(function($) {
  'use strict';
  
	jQuery(document).ready(function(){
    
		//Header
		jQuery(window).on("load resize scroll", function(e) {
			var Win = $(window).height();
			var Header = $("header").height();
			var Footer = $("footer").height();
			var NHF = Header + Footer;
			jQuery('.main').css('min-height', (Win - NHF));
		
		});
		
		//Textarea 
		jQuery('.firstCap, textarea').on('keypress', function(event) {
			var jQuerythis = $(this),
				thisVal = jQuerythis.val(),
				FLC = thisVal.slice(0, 1).toUpperCase(),
			con = thisVal.slice(1, thisVal.length);
			jQuery(this).val(FLC + con);
		});
	});

	//Custom Scrollbar

	jQuery(window).on("load",function(){
		jQuery(".result-wrapper .grid_item ul").mCustomScrollbar();
	});
	
	// Date Picker
  
    jQuery( "#datepicker" ).datepicker();

	//Page Zoom
	document.documentElement.addEventListener('touchstart', function (event) {
		if (event.touches.length > 1) {
			event.preventDefault();
		}
	}, false);

	//Avoid pinch zoom on iOS
	document.addEventListener('touchmove', function(event) {
		if (event.scale !== 1) {
			event.preventDefault();
		}
	}, false);
})(jQuery)