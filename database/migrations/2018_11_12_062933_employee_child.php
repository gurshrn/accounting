<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeChild extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_child', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id',255);
            $table->string('child_name',255);
            $table->date('child_date_of_birth');
            $table->string('attached_doc', 255);
            $table->boolean('attending_school');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_child');
    }
}
