<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkingScheduleAdjustments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_schedule_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('work_schedule_id');
            $table->integer('emp_id');
            $table->integer('days_count');
            $table->text('week_days');
            $table->integer('added_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('working_schedule_adjustments');
    }
}
