<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HardwareMaintenance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hardware_maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hw_id');
            $table->integer('location_id');
            $table->text('problem_description');
            $table->date('date');
            $table->enum('resolve',['Yes','No']);
            $table->date('resolve_date');
            $table->text('tech_info');
            $table->integer('resolved_by');
            $table->integer('added_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hardware_maintenances');
    }
}
