<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HardwareRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hardware_registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hw_id');
            $table->string('device');
            $table->integer('location_id');
            $table->string('description',255);
            $table->float('price');
            $table->date('date_of_purchase');
            $table->integer('added_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hardware_registrations');
    }
}
