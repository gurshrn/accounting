<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Employee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id',255);
            $table->integer('emp_type_id');
            $table->string('emp_name', 255);
            $table->enum('gender', ['Female','Male']);
            $table->date('date_of_birth');
            $table->string('place_of_birth',255);
            $table->string('nationality',255);
            $table->string('address',255);
            $table->string('job_title',255);
            $table->enum('wage_type',['Monthly','By Monthly']);
            $table->double('wage');
            $table->enum('martial_status',['Married','Single','Divorced','Widowed']);
            $table->string('attached_doc',255);
            $table->string('spouse_name',255);
            $table->date('spouse_date_of_birth');
            $table->integer('added_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
